@extends('layouts.test')
@section('content')
    <div class="login-box">
        <div class="login-logo">
            <div class="login-logo">
                Введите сумма
            </div>
        </div>
        <div class="card">
            <div class="card-body login-card-body">

                <div class="form-group">
                    @csrf
                    <input type="number" id="pay_text" class="form-control" value="{{ $money }}" required placeholder="Введите сумму для оплаты">
                    <button id="checkout" class="btn btn-primary btn-block btn-flat mt-3">
                        Оплатить
                    </button>
                </div>
            </div>

        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://widget.cloudpayments.ru/bundles/cloudpayments"></script>
    <script>
        $('#checkout').click(() => {
            let pay_text = $('#pay_text').val();
            if(pay_text.trim() === ''){
                alert('Указана не верная сумма');
                return false;
            }
            let paysum = parseFloat($('#pay_text').val());
            if(paysum <= 0){
                alert('Указана не верная сумма');
                return false;
            }

            let widget = new cp.CloudPayments();
            widget.pay('auth', // или 'charge'
                { //options
                    publicId: '{{ $payment_user }}', //id из личного кабинета
                    description: 'Оплата услуг в bezrieltora.kz', //назначение
                    amount: paysum, //сумма
                    currency: 'KZT', //валюта
                    accountId: '{{ $token }}', //идентификатор плательщика (необязательно)
                    invoiceId: '1234567', //номер заказа  (необязательно)
                    email: '', //email плательщика (необязательно)
                    skin: "classic", //дизайн виджета (необязательно)
                    data: {
                        token: ''
                    }
                },
                {
                    onSuccess: function (options) { // success
                        console.log('onSuccess')
                        console.log(options)
                    },
                    onFail: function (reason, options) { // fail
                        if(reason !== 'User has cancelled') {
                            alert(reason);
                            return false;
                        }
                    },
                    onComplete: function (paymentResult, options) { //Вызывается как только виджет получает от api.cloudpayments ответ с результатом транзакции.
                        if(!paymentResult.success){
                            alert(paymentResult.message);
                            return false;
                        }
                        let csrf = $('input[name=_token]').val();
                        let params = {
                            "_token" : csrf,
                            "params" : options
                        }
                        $.post('{{ asset('payments/save') }}', params, function(data){
                            alert(data.message);
                        });
                        // console.log('onComplete');
                        // console.log(paymentResult);
                        // console.log(options)
                        //4405639769294933
                    }
                }
            )
        });

        $(window).on('load', function () {
            $('#checkout').click();
        });
    </script>
@endsection
